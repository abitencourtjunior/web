from flask import Flask, render_template, request
from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer
import os

# Criando o chatbot
bot = ChatBot("Altabot")

# Definindo professor
bot.set_trainer(ListTrainer)

for sabedoria in os.listdir("base"):
    memoria = open('base/'+sabedoria, 'r').readlines()
    bot.train(memoria)

app = Flask(__name__)

@app.route("/home")
def index():
    return render_template('index.html')

@app.route("/processar", methods=['POST'])
def processar():
    usuario_entrada = request.form['usuario_entrada']
    bot_response = bot.get_response(usuario_entrada)
    bot_response = str(bot_response)
    print("Altabot: "+bot_response)
    return render_template('index.html', usuario_entrada=usuario_entrada,
    bot_response=bot_response)

if __name__ == '__main__':
    app.run()