class ProdutoDao{

  constructor(db){
    this._db = db;
  }

  lista(){
    return new Promise((resolve, reject) =>{
      this._db.all(
        'SELECT * FROM produtos;',
          (error, result) => {
            if (error) return reject('Falha ao buscar produtos!!');

            return resolve(result);
          });
        }
      });

}
