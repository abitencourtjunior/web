let pacientes = document.querySelectorAll('.paciente');

for(let cont=0; cont < pacientes.length; cont++) {

    let paciente = pacientes[cont];

    let tdAltura = paciente.querySelector(".info-altura");
    let altura = tdAltura.textContent;

    let tdPeso = paciente.querySelector(".info-peso");
    let peso = tdPeso.textContent;

    let tdImc = paciente.querySelector(".info-imc");
    let imc;

    let pesoValido = true;
    let alturaValida = true;

    //!pesoValido ? (tdImc.textContent = "Peso inválido!" , pesoValido = false, paciente.classList.add("paciente-invalido")) : pesoValido = true;
    //!alturaValida ? (tdImc.textContent = "Altura Inválida!" , alturaValida = false, paciente.classList.add("paciente-invalido")) : alturaValida = true;

    if (peso <= 0 || peso >= 1000) {
        pesoValido = false;
        tdImc.textContent = "Peso inválido";
        paciente.classList.add("paciente-invalido");
    }

    if (altura <= 0 || altura >= 3.00) {
        alturaValida = false;
        tdImc.textContent = "Altura inválida";
        paciente.classList.add("paciente-invalido");
    }

    if (pesoValido && alturaValida) {
        imc = calculaImc(peso, altura);
        tdImc.textContent = imc;
    }

}

function calculaImc(peso, altura) {

    let imc = peso / (altura * altura);

    return imc.toFixed(2);
}

function validaPeso(peso) {
    if (peso >= 0 && peso < 800){
        return true;
    }
    return false;
}

function validaaltura(altura) {
    if(altura >=0 && altura < 3.0){
        return true;
    }
    return false;
}