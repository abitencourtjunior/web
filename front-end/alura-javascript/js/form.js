
let botaoformulario = document.querySelector("#adicionar-paciente");

botaoformulario.addEventListener("click", function (event) {

    event.preventDefault();

    let form = document.querySelector("#form-adiciona");
    let paciente = infoPaciente(form);

    let erros = validaPaciente(paciente);

    if(erros.length > 0){
        
        mensagemDeErro(erros);
        return;
    }

    adicionarPacienteTabela(paciente);

    form.reset();

    var mensagensErro = document.querySelector("#mensagens-erro");
    mensagensErro.innerHTML = "";

});

function adicionarPacienteTabela(paciente) {

    let pacienteTr = montaTr(paciente);

    let tabela = document.querySelector("#tabela-pacientes");

    tabela.appendChild(pacienteTr);
}

function infoPaciente(form){

    var paciente = {
        nome:form.nome.value,
        peso: form.peso.value,
        gordura: form.gordura.value,
        altura : form.altura.value,
        imc: calculaImc(form.peso.value, form.altura.value)
    }

    return paciente

}

function montaTr(paciente) {

    let pacienteTr = document.createElement("tr");
    pacienteTr.classList.add("paciente");

    pacienteTr.appendChild(montaTd(paciente.nome, "info-nome"));
    pacienteTr.appendChild(montaTd(paciente.peso, "info-peso"));
    pacienteTr.appendChild(montaTd(paciente.altura, "info-altura"));
    pacienteTr.appendChild(montaTd(paciente.gordura, "info-gordura"));
    pacienteTr.appendChild(montaTd(paciente.imc, "info-imc"));

    return pacienteTr;

}

function montaTd(dados, classe) {
    let td = document.createElement('td');
    td.textContent = dados;
    td.classList.add(classe);

    return td;

}

function validaPaciente(paciente) {

    var erros = [];

    if (paciente.nome.length == 0) {
        erros.push("O nome não pode ser em branco");
    }

    if (paciente.gordura.length == 0) {
        erros.push("A gordura não pode ser em branco");
    }

    if (paciente.peso.length == 0) {
        erros.push("O peso não pode ser em branco");
    }

    if (paciente.altura.length == 0) {
        erros.push("A altura não pode ser em branco");
    }

    if (!validaPeso(paciente.peso)) {
        erros.push("Peso é inválido");
    }

    return erros;

}

function mensagemDeErro(erros) {

    var ul = document.querySelector("#mensagens-erro");

    erros.forEach(function (erros) {
       let li = document.createElement("li");
       li.textContent = erros;
       ul.appendChild(li);
       //  alert(erros);
    });

}